import { fizzbuzz_oneif } from "./fizzbuzz_one_if";

describe("Fizzbuzz", () => {
    it("should return 1 when input 1", () => {
        expect(fizzbuzz_oneif(1)).toEqual("1");
    })
    it("should return 2 when input 2", () => {
        expect(fizzbuzz_oneif(2)).toEqual("2");
    })
    it("should return Fizz when input 3", () => {
        expect(fizzbuzz_oneif(3)).toEqual("Fizz");
    })
    it("should return 4 when input 4", () => {
        expect(fizzbuzz_oneif(4)).toEqual("4");
    })
    it("should return Buzz when input 5", () => {
        expect(fizzbuzz_oneif(5)).toEqual("Buzz");
    })
    it("should return Fizz when input 6", () => {
        expect(fizzbuzz_oneif(6)).toEqual("Fizz");
    })
    it("should return 7 when input 7", () => {
        expect(fizzbuzz_oneif(7)).toEqual("7");
    })
    it("should return 8 when input 8", () => {
        expect(fizzbuzz_oneif(8)).toEqual("8");
    })
    it("should return Fizz when input 9", () => {
        expect(fizzbuzz_oneif(9)).toEqual("Fizz");
    })
    it("should return Buzz when input 10", () => {
        expect(fizzbuzz_oneif(10)).toEqual("Buzz");
    })
    it("should return 11 when input 11", () => {
        expect(fizzbuzz_oneif(11)).toEqual("11");
    })
    it("should return Fizz when input 12", () => {
        expect(fizzbuzz_oneif(12)).toEqual("Fizz");
    })
    it("should return 13 when input 13", () => {
        expect(fizzbuzz_oneif(13)).toEqual("13");
    })
    it("should return 14 when input 14", () => {
        expect(fizzbuzz_oneif(14)).toEqual("14");
    })
    it("should return FizzBuzz when input 15", () => {
        expect(fizzbuzz_oneif(15)).toEqual("FizzBuzz");
    })
});