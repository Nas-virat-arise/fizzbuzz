

export const fizzbuzz_oneif = (num: number): string => {
    let map = new Map();
    map.set(3,"Fizz")
    map.set(5,"Buzz")
    map.set(6,"Fizz")
    map.set(9,"Fizz")
    map.set(10,"Buzz")
    map.set(12,"Fizz")
    map.set(15,"FizzBuzz")
    if(num%3 == 0 || num%5 == 0){
        return map.get(num)
    } else{
        return num.toString()
    }
}