export const fizzbuzz = (num:number):string => {
    if(num == 15){
        return "FizzBuzz"
    }
    if(num%3 == 0){
        return "Fizz"
    } else if(num%5 != 0){
        return num.toString()
    } else{
        return "Buzz"
    }
}